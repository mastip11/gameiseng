package com.mastip.gameiseng;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;

public class PurchaseActivity extends AppCompatActivity {

    RecyclerView listPurchaseItem;
    TextView tvMoney;

    ArrayList<UserPurchaseInfo> userPurchaseInfos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);

        generatingItem();

        listPurchaseItem = (RecyclerView) findViewById(R.id.listPurchaseItem);
        tvMoney = (TextView) findViewById(R.id.tvPriceOnPurchase);

        tvMoney.setText("$" + new ValueHolder().getMoney());

        PurchaseCustomAdapter adapter = new PurchaseCustomAdapter(this, userPurchaseInfos);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listPurchaseItem.setLayoutManager(layoutManager);
        listPurchaseItem.setItemAnimator(new DefaultItemAnimator());
        listPurchaseItem.setAdapter(adapter);
    }

    private void generatingItem() {
        if(new ItemPurchasedCounter().getUserPurchaseInfos().size() > 0) {
            userPurchaseInfos = new ItemPurchasedCounter().getUserPurchaseInfos();
        }
        else {
            userPurchaseInfos.add(new UserPurchaseInfo(1, "Time Adding", "Add time for 3 seconds", 0, 4, 25, 3000));
            userPurchaseInfos.add(new UserPurchaseInfo(2, "Sharpen Bamboo", "A small weapon. Add damage for 2 points", 0, 7, 8, 2));
            userPurchaseInfos.add(new UserPurchaseInfo(3, "Knife", "Still a small weapon. Add damage for 4 points", 0, 7, 13, 4));
            userPurchaseInfos.add(new UserPurchaseInfo(4, "Gun", "I have been warned you, boss. Add damage for 7 points", 0, 10, 19, 7));
            userPurchaseInfos.add(new UserPurchaseInfo(5, "Samurai", "Whoops, be careful, boss. Add damage for 10 points", 0, 10, 25, 10));
            userPurchaseInfos.add(new UserPurchaseInfo(6, "Shotgun", "Lol do u wanna die boss? Add damage for 15 points", 0, 12, 32, 15));
        }
    }
}
