package com.mastip.gameiseng;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Tivo Yudha on 14/04/2017.
 */

public class PurchaseCustomAdapter extends RecyclerView.Adapter<PurchaseCustomAdapter.CustomViewHolder> {

    Context context;

    ArrayList<UserPurchaseInfo> userPurchaseInfos = new ArrayList<>();

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        public TextView tvItemTitle, tvItemDesc, tvItemPrice, tvItemValue;
        public Button buttonPurchase;

        public CustomViewHolder(View v) {
            super(v);
            tvItemTitle = (TextView) v.findViewById(R.id.tvItemName);
            tvItemDesc = (TextView) v.findViewById(R.id.tvDescription);
            tvItemPrice = (TextView) v.findViewById(R.id.tvPriceItem);
            tvItemValue = (TextView) v.findViewById(R.id.tvLevelItem);
            buttonPurchase = (Button) v.findViewById(R.id.buttonPurchase);
        }
    }

    public PurchaseCustomAdapter(Context context, ArrayList userPurchaseInfos) {
        this.context = context;
        this.userPurchaseInfos = userPurchaseInfos;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_purchase_item_layout, null);

        CustomViewHolder vh = new CustomViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position) {
        final UserPurchaseInfo info = userPurchaseInfos.get(position);

        final int itemId = info.getId();

        holder.tvItemTitle.setText(info.getItemName());
        holder.tvItemDesc.setText(info.getDescription());
        holder.tvItemValue.setText(" (" + info.getNowValue() + "/" + info.getMaxValue() + ")");
        holder.tvItemPrice.setText("$" + info.getPriceItem());
        holder.buttonPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(purchaseValidationFromValue(info.getNowValue(), info.getMaxValue())) {
                    if(purchaseValidationFromPrice(new ValueHolder().getMoney(), info.getPriceItem())) {
                        processPurchasing(itemId, new ValueHolder().getMoney(), info.getPriceItem(), info.getPointIncrement());
                        info.setNowValue(info.getNowValue() + 1);
                        notifyDataSetChanged();
                        backupData();
                    } else {
                        Toast.makeText(context, "You have no money for this item", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(context, "You have reached max value from this item", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return userPurchaseInfos.size();
    }

    private void backupData() {
        new ItemPurchasedCounter().setUserPurchaseInfos(userPurchaseInfos);
    }

    private boolean purchaseValidationFromValue(int nowValue, int maxValue) {
        if(nowValue >= maxValue) {
            return false;
        }
        else {
            return true;
        }
    }

    private boolean purchaseValidationFromPrice(int money, int price) {
        if(money >= price) {
            return true;
        }
        else {
            return false;
        }
    }

    private void processPurchasing(int id, int money, int price, int increment) {
        money -= price;
        new ValueHolder().setMoney(money);

        updatingMoneyScreen();

        if(id == 1) {
            timeIncrement(id, increment);
        }
        else {
            damageIncrement(increment);
        }
    }

    private void timeIncrement(int id, int increment) {
        int timeIncrement = new ValueHolder().getTimeIncrease();
        timeIncrement += increment;
        new ValueHolder().setTimeIncrease(timeIncrement);
    }

    private void damageIncrement(int increment) {
        int damageIncr = new ValueHolder().getDamage();
        damageIncr += increment;
        new ValueHolder().setDamage(damageIncr);
    }

    private void updatingMoneyScreen() {
        View vi = LayoutInflater.from(context).inflate(R.layout.activity_purchase, null);
        TextView tvMoney = (TextView) vi.findViewById(R.id.tvPriceOnPurchase);

        tvMoney.setText("$" + new ValueHolder().getMoney());
    }
}
