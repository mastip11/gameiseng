package com.mastip.gameiseng;

import java.util.ArrayList;

/**
 * Created by Tivo Yudha on 14/04/2017.
 */

public class UserPurchaseInfo {

    private int id;
    private String itemName;
    private String description;
    private int nowValue;
    private int maxValue;
    private int priceItem;
    private int pointIncrement;

    public UserPurchaseInfo() {

    }

    public UserPurchaseInfo(int id, String itemName, String description, int nowValue, int maxValue, int priceItem, int pointIncrement) {
        this.id = id;
        this.itemName = itemName;
        this.description = description;
        this.nowValue = nowValue;
        this.maxValue = maxValue;
        this.priceItem = priceItem;
        this.pointIncrement = pointIncrement;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public void setNowValue(int nowValue) {
        this.nowValue = nowValue;
    }

    public void setPriceItem(int priceItem) {
        this.priceItem = priceItem;
    }

    public void setPointIncrement(int pointIncrement) {
        this.pointIncrement = pointIncrement;
    }

    public int getId() {
        return id;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public int getNowValue() {
        return nowValue;
    }

    public int getPriceItem() {
        return priceItem;
    }

    public String getDescription() {
        return description;
    }

    public String getItemName() {
        return itemName;
    }

    public int getPointIncrement() {
        return pointIncrement;
    }
}
