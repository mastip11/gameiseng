package com.mastip.gameiseng;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onGameStart(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void onPurchaseItem(View view) {
        Intent intent = new Intent(this, PurchaseActivity.class);
        startActivity(intent);
    }

    public void onCreditClicked(View view) {
        AlertDialog.Builder alertCredit = new AlertDialog.Builder(this);
        alertCredit.setTitle("Credits");
        alertCredit.setMessage("This game was created by Tivo Yudha P. This game also for educational purpose");
        alertCredit.show();
    }
}
