package com.mastip.gameiseng;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity {

    RelativeLayout layoutGameScreen;
    TextView tvCounter, tvBoss, tvBossLevel, tvBossLife, tvTimer, tvMoneyEarned;

    private int a = 4;
    private static int displayX;
    private static int displayY;
    private int bossPositionX = 0;
    private int bossPositionY = 0;
    private int randomValue = 0;
    private int bossLevel = 1;
    private int bossLife = 100;
    private int damagePerClick = 10;
    private int bossLifeInThisLevel = 100;
    private int bossLifeIncrement = 50;
    private boolean canUserClick = true;
    private long timer = 20000;
    private long timerIncrement = 0;
    private int nowMoney = 0;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        damagePerClick = new ValueHolder().getDamage();
        nowMoney = new ValueHolder().getMoney();
        timerIncrement = new ValueHolder().getTimeIncrease();

        tvCounter = (TextView) findViewById(R.id.tvCounter);
        tvBoss = (TextView) findViewById(R.id.tvTheBoss);
        layoutGameScreen = (RelativeLayout) findViewById(R.id.layoutGameScreen);
        tvBossLevel = (TextView) findViewById(R.id.tvBossLevel);
        tvBossLife = (TextView) findViewById(R.id.tvBossLife);
        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvMoneyEarned = (TextView) findViewById(R.id.tvMoneyEarned);

        settingBossInfo();
        counterBeforeStart();
    }

    public void onClickBoss(View view) {
        if(canUserClick) {
            onBossClicked();
            setMoneyEarned();
        }
    }

    public void setMoneyEarned() {
        nowMoney += bossLevel;
        new ValueHolder().setMoney(nowMoney);
        tvMoneyEarned.setText("$" + nowMoney);
    }

    private void counterBeforeStart() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    do {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvCounter.setText("" + a);
                            }
                        });
                        a--;
                        Thread.sleep(1000);
                    } while(a > 1);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvCounter.setVisibility(View.GONE);
                            showingTheBoss();
                            setTimer();
                        }
                    });

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void gettingGameScreenXY() {
        displayX = layoutGameScreen.getWidth();
        displayY = layoutGameScreen.getHeight();
    }

    private void showingTheBoss() {
        if(tvBoss.getVisibility() != View.VISIBLE) {
            tvBoss.setVisibility(View.VISIBLE);
        }

        gettingGameScreenXY();

        bossPositionX = generateRandomNumber(displayX);
        bossPositionY = generateRandomNumber(displayY);

        tvBoss.animate().x((float) bossPositionX).y((float) bossPositionY).setDuration(300).start();
    }

    private int generateRandomNumber(int params) {
        randomValue = 0;
        Random generateBossLocation = new Random();

        do {
            randomValue = generateBossLocation.nextInt(params + 1);
        } while(randomValue >= params || randomValue > (params - 50));

        return randomValue;
    }

    private void onBossClicked() {
        bossLife-=damagePerClick;

        settingBossInfo();
        checkingIfBossIsDefeated();

        if(checkingIfBossIsDefeated()) {
            canUserClick = false;
            tvBoss.setVisibility(View.INVISIBLE);

            showingAlert();
        }
        else {
            showingTheBoss();
        }
    }

    private boolean checkingIfBossIsDefeated() {
        if(bossLife <= 0) {
            countDownTimer.cancel();
            return true;
        }
        else {
            return false;
        }
    }

    private void showingAlert() {
        AlertDialog.Builder alertBossIsDefeated = new AlertDialog.Builder(this);
        alertBossIsDefeated.setTitle("Congratulation!");
        alertBossIsDefeated.setMessage("The level " + bossLevel + " Boss has been defeated");
        alertBossIsDefeated.setPositiveButton("NEXT STAGE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                settingNextLevelBoss();

                canUserClick = true;

                showingTheBoss();
                setTimer();
            }
        });
        alertBossIsDefeated.setNegativeButton("END", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
                MainActivity.this.finish();
            }
        });
        alertBossIsDefeated.setCancelable(false);
        alertBossIsDefeated.show();
    }

    private void settingNextLevelBoss() {
        bossLevel += 1;
        bossLifeInThisLevel += bossLifeIncrement;
        bossLife = bossLifeInThisLevel;
        timer += timerIncrement;

        settingBossInfo();
    }

    private void settingBossInfo() {
        tvBossLevel.setText("" + bossLevel);
        tvBossLife.setText("" + bossLife);
    }

    @Override
    public void onBackPressed() {
        // do nothing
    }

    private void setTimer() {
        countDownTimer = new CountDownTimer(timer, 1000) {
            @Override
            public void onTick(long l) {
                timer = l;
                tvTimer.setText("" + l / 1000 + " s");
            }

            @Override
            public void onFinish() {
                tvTimer.setText("0 s");
                canUserClick = false;

                if(checkingIfBossIsDefeated()) {
                    showingAlert();
                }
                else {
                    gameOverAlert();
                }
            }
        };
        countDownTimer.start();
    }

    private void gameOverAlert() {
        AlertDialog.Builder alertGameOver = new AlertDialog.Builder(this);
        alertGameOver.setTitle("Game Over!");
        alertGameOver.setMessage("You cannot defeat the boss before time ends");
        alertGameOver.setPositiveButton("BACK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(intent);
                MainActivity.this.finish();
            }
        });
        alertGameOver.setCancelable(false);
        alertGameOver.show();
    }
}
