package com.mastip.gameiseng;

import java.util.ArrayList;

/**
 * Created by Tivo Yudha on 14/04/2017.
 */

public class ItemPurchasedCounter {

    private static ArrayList<UserPurchaseInfo> userPurchaseInfos = new ArrayList<>();

    public static void setUserPurchaseInfos(ArrayList<UserPurchaseInfo> userPurchaseInfos) {
        ItemPurchasedCounter.userPurchaseInfos = userPurchaseInfos;
    }

    public static ArrayList<UserPurchaseInfo> getUserPurchaseInfos() {
        return userPurchaseInfos;
    }
}
