package com.mastip.gameiseng;

/**
 * Created by Tivo Yudha on 14/04/2017.
 */

public class ValueHolder {

    private static int Money = 0;
    private static int TimeIncrease = 4000;
    private static int Damage = 10;

    public static void setDamage(int damage) {
        Damage = damage;
    }

    public static void setMoney(int money) {
        Money = money;
    }

    public static void setTimeIncrease(int timeIncrease) {
        TimeIncrease = timeIncrease;
    }

    public static int getMoney() {
        return Money;
    }

    public static int getTimeIncrease() {
        return TimeIncrease;
    }

    public static int getDamage() {
        return Damage;
    }
}
